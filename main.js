// 1) В js существует 8 типов данных.

// 1. string (строка) заключается в кавычки ("", '', ``-кавычки в которые можно вставлять переменные)

// 2. number (число) может представлять собой как целые так и с плавающей точкой числа

// 3. biglnt тип данных для больших чисел

// 4. undefined тип данных, который говорит о том , что переменной не было присвоено значение

// 5. null тип данных который обозначает , что в переменной «пусто»

// 6. boolean тип данных который принимает в себя только true или false

// 7. typeof предназначен для того чтобы определить какой перед нами тип данных. Например: typeof "123" == "string"

// 8. object тип данных создан для описания или хранения более сложных структур


// 2) == нестрогое равенство, используется для сравнения переменных, игнорирую при этом тип данных 

// === строгое равенство для сравнения переменных, но при этом проверяет тип данных значений которые сравниваются между собой


// 3) оператор может выполнять какие-либо действия над операндом. Например 2 + 2, оператор + складывает два операнда


let nameInfo;
let ageInfo;

let question;

do {
    if (!nameInfo) {
        nameInfo = prompt("What is your name?");
    }
    if (!ageInfo) {

        ageInfo = +prompt("What is your age?");
    }
} while (
    !nameInfo || !ageInfo
)


if (ageInfo < 18) {
    alert("You are not allowed to visit this website");
} else if (ageInfo >= 18 && ageInfo <= 22) {
    question = confirm("Are you sure you want to continue?");
    (question == true) ?
    alert("Welcome"):
        alert("You are not allowed to visit this website");
} else {
    alert("Welcome " + nameInfo);
}